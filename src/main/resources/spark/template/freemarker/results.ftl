<#include "header.ftl">

	<div class="row" id="searchform">
	  <form action="/zoek">
		<div class="eight columns">
			<input class="u-full-width" value="${searchQuery!}" placeholder="Bijv: Familienaam, voornaam, plantage, scheepsnaam, district" name="searchQuery" id="searchQuery" type="text">
		</div>
		<div class="two columns">
			<input class="button-primary" value="Zoeken" type="submit">
		</div>
	  </form>
	</div>

    <!-- Lists -->
    <div class="docs-section" id="lists">
      <h6 class="docs-header">Zoekresultaten</h6>
	  <div class="row results-header">
		<p>1-${searchResults.pageSize} van ${searchResults.totalCount} zoekresultaten</p>
	  </div>
	  <#list searchResults.results as searchResult>
       <div class="row results-section">
        <div class="eight columns">
		  <a href="${searchResult.link!}"><h5>${searchResult.person.surname!}, ${searchResult.person.firstNames!}</h5></a>
		  <p>${searchResult.summary!}</p>
        </div>
       </div>
	  </#list>
	  
  	  <div class="row results-footer">
		<a class="button button-primary" href="#" onclick="false">Meer zoekresultaten</a>		
	  </div>
    </div>

<#include "footer.ftl">