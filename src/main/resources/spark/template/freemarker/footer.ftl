    <!-- About section -->
    <div class="docs-section" id="over">
      <h6 class="docs-header">Over deze site</h6>
      <p>SuriRoots is een site die het makkelijker maakt om gegevens te vinden over je familie die destijds is overgekomen 
		 vanuit India naar Suriname. We maken alleen gebruik van informatie die publiekelijk te vinden is en bieden
		 een zoek interface aan naar deze data. De data zelf is samengesteld door de mensen van <a href="http://www.gahetna.nl/">gahetna.nl</a>.
	  </p>
	  <p>Wij zijn niet verantwoordelijk voor eventuele fouten in de data.</p>
	  <p>Als u contact wilt opnemen met de maker van deze site, gebruik dan het <a href="#contact">contact formulier</a></p>.
   
    <p>Credits:</p>
      <div>Icon made by <a href="http://www.simpleicon.com" title="SimpleIcon">SimpleIcon</a> from <a href="http://www.flaticon.com" title="Flaticon">www.flaticon.com</a> is licensed under <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0">CC BY 3.0</a></div>
      <div>Icons made by <a href="http://www.freepik.com" title="Freepik">Freepik</a> from <a href="http://www.flaticon.com" title="Flaticon">www.flaticon.com</a> is licensed under <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0">CC BY 3.0</a></div>
    </div>
	
    <!-- contact -->
    <div class="docs-section" id="contact">
      <h6 class="docs-header">Contact</h6>
      <p>Heb je vragen over deze site? Gebruik dan onderstaand formulier om een mailtje te sturen naar de eigenaar.</p>
      <div class="docs-example docs-example-forms">
        <form>
          <div class="row">
            <div class="six columns">
              <label for="exampleEmailInput">Je email adres</label>
              <input class="u-full-width" placeholder="jouw@emailadres.com" id="exampleEmailInput" type="email">
            </div>
            <div class="six columns">
              <label for="exampleRecipientInput">Reden</label>
              <select class="u-full-width" id="exampleRecipientInput">
                <option selected="selected" value="Option 1">Vraag</option>
                <option value="Option 2">Klacht</option>
                <option value="Option 3">Advertentie mogelijkheden</option>
              </select>
            </div>
          </div>
          <label for="exampleMessage">Bericht</label>
          <textarea class="u-full-width" placeholder="Hallo �" id="exampleMessage"></textarea>
          <label class="example-send-yourself-copy">
            <input type="checkbox">
            <span class="label-body">Stuur een kopie naar jezelf</span>
          </label>
          <input class="button-primary" value="Submit" type="submit">
        </form>
      </div>
    </div>
  </div>

  <script src="js/auto-complete.min.js"></script>
  <script>
        var demo1 = new autoComplete({
            selector: '#searchBox',
            minChars: 1,
            source: function(term, suggest){
                term = term.toLowerCase();
                var choices = ['Anoep', 'Koesal', 'Tikai', 'Assembly', 'BASIC', 'Batch', 'C', 'C++', 'CSS', 'Clojure', 'COBOL', 'ColdFusion', 'Erlang', 'Fortran', 'Groovy', 'Haskell', 'HTML', 'Java', 'JavaScript', 'Lisp', 'Perl', 'PHP', 'PowerShell', 'Python', 'Ruby', 'Scala', 'Scheme', 'SQL', 'TeX', 'XML'];
                var suggestions = [];
                for (i=0;i<choices.length;i++)
                    if (~choices[i].toLowerCase().indexOf(term)) suggestions.push(choices[i]);
                suggest(suggestions);
            }
        });
   </script>
   <!-- End Document
  �������������������������������������������������� -->
</body>

</html>
