<!DOCTYPE html>
<html lang="en"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">

  <!-- Basic Page Needs
  末末末末末末末末末末末末末末末末末末末末末末末末末 -->
  <meta charset="utf-8">
  <title>SuriRoots: Op zoek naar je Surinaams Hindoestaanse familie gegevens</title>
  <meta name="description" content="Vind historische gegevens over Afrikaanse, Javaanse en Indiase immigranten in Suriname">
  <meta name="author" content="Shanny Anoep">

  <!-- Mobile Specific Metas
  末末末末末末末末末末末末末末末末末末末末末末末末末 -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT
  末末末末末末末末末末末末末末末末末末末末末末末末末 -->
  <link href="css/css.css" rel="stylesheet" type="text/css">

  <!-- CSS
  末末末末末末末末末末末末末末末末末末末末末末末末末 -->
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/skeleton.css">
  <link rel="stylesheet" href="css/custom.css">
  <link rel="stylesheet" href="css/auto-complete.css">
  
  <!-- Scripts
  末末末末末末末末末末末末末末末末末末末末末末末末末 -->
  <script src="js/jquery.js"></script>
  <script src="js/site.js"></script>

  <!-- Favicon
  末末末末末末末末末末末末末末末末末末末末末末末末末 -->
  <link rel="icon" type="image/png" href="http://getskeleton.com/dist/images/favicon.png">

</head>
<body data-twttr-rendered="true" class="code-snippets-visible has-docked-nav">

  <!-- Primary Page Layout
  末末末末末末末末末末末末末末末末末末末末末末末末末 -->
  <div class="container">
  
    <div class="navbar-spacer"></div>
    <nav class="navbar">
      <div class="container">		
        <ul class="navbar-list">
          <li class="navbar-item"><a class="navbar-link" href="/"><span class="navbar-logo">Suri</span>Roots</a></li>
          <li class="navbar-item"><a class="navbar-link" href="#searchform">Zoeken</a></li>
          <li class="navbar-item"><a class="navbar-link" href="#over">Over</a></li>
          <li class="navbar-item"><a class="navbar-link" href="#contact">Contact</a></li>
        </ul>
      </div>
    </nav>
