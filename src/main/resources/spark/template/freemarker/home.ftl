<#include "header.ftl">

	<section class="header" id="searchform">
      <h2 class="title">Vind je familie</h2>
	  <form action="/zoek">
		<input class="u-full-width" value="${searchQuery!}" placeholder="Bijv: Familienaam, voornaam, plantage, scheepsnaam, district" name="searchQuery" id="searchQuery" type="text">
		<input class="button-primary" value="Zoeken" type="submit">
	  </form>
      <div class="value-props row">
        <div class="four columns value-prop">
          <img class="value-img" src="img/network65.svg">
          Snel gegevens vinden over immigranten in Suriname. Voorlopig alleen informatie beschikbaar over Hindoestaanse immigranten.
        </div>
        <div class="four columns value-prop">
          <img class="value-img" src="img/facebook29.svg">
          Deel de gevonden informatie met je eigen familie via Facebook.
        </div>
        <div class="four columns value-prop">
          <img class="value-img" src="img/map32.svg">
          Vind de geboorteplaats van je voorouders op Google Maps.
        </div>
      </div>
    </section>
	
	<!-- Lists -->
    <div class="docs-section" id="lists">
      <h6 class="docs-header">Populair</h6>
      <div class="row docs-example">
        <div class="six columns">
		  <h6>Meest gezocht</h6>
          <ol>
            <li>Anoep</li>
			<li>Tikai</li>
			<li>Koesal</li>
          </ol>
        </div>
        <div class="six columns">
		  <h6>Meest gedeeld</h6>
          <ul>
            <li>Anoep</li>
			<li>Tikai</li>
			<li>Koesal</li>
          </ul>
        </div>
      </div>
    </div>



<#include "footer.ftl">
