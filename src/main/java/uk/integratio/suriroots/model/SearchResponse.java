package uk.integratio.suriroots.model;

import java.util.List;

import uk.integratio.suriroots.model.SearchResult;

public class SearchResponse {
	public static int DEFAULT_PAGE_SIZE = 10;

	private List<SearchResult> results;
	private int pageSize = DEFAULT_PAGE_SIZE;
	private int totalCount;

	public int getTotalCount() {
		return this.totalCount;
	}

	public List<SearchResult> getResults() {
		return this.results;
	}

	public int getPageSize() {
		return this.pageSize;
	}

	public static class Builder {
		private List<SearchResult> results;
		private int pageSize;
		private int totalCount;

		public Builder results(List<SearchResult> results) {
			this.results = results;
			return this;
		}

		public Builder pageSize(int pageSize) {
			this.pageSize = pageSize;
			return this;
		}

		public Builder totalCount(int totalCount) {
			this.totalCount = totalCount;
			return this;
		}

		public SearchResponse build() {
			return new SearchResponse(this);
		}
	}

	private SearchResponse(Builder builder) {
		this.results = builder.results;
		this.pageSize = builder.pageSize;
		this.totalCount = builder.totalCount;
	}
}
