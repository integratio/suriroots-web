package uk.integratio.suriroots.model;

import uk.integratio.suriroots.model.Person;

public class SearchResult {
	private Person person;
	private String link;
	private String summary;

	public SearchResult() {
	}

	public Person getPerson() {
		return this.person;
	}

	public String getLink() {
		return this.link;
	}

	public String getSummary() {
		return this.summary;
	}

	public static class Builder {
		private Person person;
		private String link;
		private String summary;

		public Builder person(Person person) {
			this.person = person;
			return this;
		}

		public Builder link(String link) {
			this.link = link;
			return this;
		}

		public Builder summary(String summary) {
			this.summary = summary;
			return this;
		}

		public SearchResult build() {
			return new SearchResult(this);
		}
	}

	private SearchResult(Builder builder) {
		this.person = builder.person;
		this.link = builder.link;
		this.summary = builder.summary;
	}
}
