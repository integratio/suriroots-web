package uk.integratio.suriroots.model;

public class Person {

	public Person() {
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public void setFirstNames(String firstNames) {
		this.firstNames = firstNames;
	}

	private String surname;
	private String firstNames;

	public String getSurname() {
		return this.surname;
	}

	public String getFirstNames() {
		return this.firstNames;
	}

	public static class Builder {
		private String surname;
		private String firstNames;

		public Builder surname(String surname) {
			this.surname = surname;
			return this;
		}

		public Builder firstNames(String firstNames) {
			this.firstNames = firstNames;
			return this;
		}

		public Person build() {
			return new Person(this);
		}
	}

	private Person(Builder builder) {
		this.surname = builder.surname;
		this.firstNames = builder.firstNames;
	}
}
