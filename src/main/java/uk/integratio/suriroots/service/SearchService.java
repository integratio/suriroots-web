package uk.integratio.suriroots.service;

import java.util.ArrayList;
import java.util.List;

import uk.integratio.suriroots.model.Person;
import uk.integratio.suriroots.model.SearchResponse;
import uk.integratio.suriroots.model.SearchResult;

public class SearchService {

  public static SearchResponse search(String query) {		  
    List<SearchResult> resultList = new ArrayList<SearchResult>();

    Person person =  new Person.Builder().firstNames("Shanny").surname("Anoep").build();		  
    String link = "/person/" + person.getSurname() + person.getFirstNames() + "/";
    String summary = "Familienaam, voornaam, geboortedatum, scheepsnaam, geboorteplaats, woonplaats";
    SearchResult result = new SearchResult.Builder().person(person).link(link).summary(summary).build();		  
    resultList.add(result);

    Person person2 =  new Person.Builder().firstNames("Anuradha").surname("Tikai").build();		  
    link = "/person/" + person2.getSurname() + person2.getFirstNames() + "/";
    summary = "Familienaam, voornaam, geboortedatum, scheepsnaam, geboorteplaats, woonplaats";
    SearchResult result2 = new SearchResult.Builder().person(person2).link(link).summary(summary).build();		  
    resultList.add(result2);

    return new SearchResponse.Builder().results(resultList).totalCount(resultList.size()).build();
  }

}
