package uk.integratio.suriroots.web;

import static spark.Spark.*;
import spark.ModelAndView;
import spark.template.freemarker.FreeMarkerEngine;
import uk.integratio.suriroots.model.Person;
import uk.integratio.suriroots.model.SearchResponse;
import uk.integratio.suriroots.service.SearchService;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

public class App 
{
  public static Map<String, Person> persons = new HashMap<String, Person>();

  public static void main( String[] args )
  {
    staticFileLocation("/public");

    get("/status", (request, response) -> "Alive!");

    get("/persons/:id", (request, response) -> {
        Person person = persons.get(request.params(":id"));
        if (person != null) {
        return "Surname: " + person.getSurname() + ", First Name(s): " + person.getFirstNames();
        } else {
        response.status(404); // 404 Not found
        return "Person not found";
        }
        });

    get("/zoek", (request, response) -> {
        System.out.println(request.params());
        System.out.println(request.queryParams());
        Map<String, Object> attributes = new HashMap<>();			
        String searchQuery = request.queryParams("searchQuery");
        SearchResponse searchResp = SearchService.search(searchQuery);

        attributes.put("searchQuery", searchQuery);
        attributes.put("searchResults", searchResp);

        // The ftl file is located in directory:
        // src/test/resources/spark/template/freemarker
        return new ModelAndView(attributes, "results.ftl");
        }, new FreeMarkerEngine());

    get("/", (request, response) -> {
        Map<String, Object> attributes = new HashMap<>();
        return new ModelAndView(attributes, "home.ftl");
        }, new FreeMarkerEngine());

  }

}
